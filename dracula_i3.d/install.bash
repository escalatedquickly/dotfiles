#!/bin/bash

I3_CONF=$HOME/.i3
TERMINATOR_CONF=$HOME/.config/terminator

if ! [[ -d $I3_CONF ]]; then
	ln -s i3 $HOME/.i3
fi

if ! [[ -d $TERMINATOR_CONFIG ]]; then
	ln -s terminator $HOME/.config/terminator
fi
