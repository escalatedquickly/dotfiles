Setup
=====
* [i3gaps]
* [polybar]
* terminator
* [cordless]


[i3gaps]:   https://github.com/Airblader/i3
[polybar]:  https://github.com/polybar/polybar
[cordless]: https://github.com/bios-marcel/cordless
